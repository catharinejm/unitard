module Tests.Inliner where

import Control.Monad.Except
import Data.String.Util
import Lang.GoML.Interregnum
import Lang.GoML.Parser
import Lang.GoML.Passes.InlineGenerics
import Lang.GoML.Passes.Renamer
import Lang.GoML.Typecheck

assertSuccess :: (MonadIO m, Stringable a) => m (Either a b) -> m b
assertSuccess action = do res <- action
                          case res of
                           Left err -> error $ tos err
                           Right res -> return res

initPackage :: (MonadIO m) => m IRPackage
initPackage = do
  source <- liftIO $ readFile "./testsources/inliner_tests.goml"
  package <- assertSuccess (parseSource source)
  env <- assertSuccess (runExceptT (buildEnv package))
  typedEnv <- assertSuccess (runTyper env)
  assertSuccess (buildIRPackage typedEnv)


runInlineTests :: (MonadIO m) => m ()
runInlineTests = do
  package <- initPackage
  return ()
