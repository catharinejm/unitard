module Tests.GIR where

import           Control.Monad
import           Control.Monad.Except
import           Control.Monad.IO.Class
import           Data.Char
import qualified Data.Map.Strict as M
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Parser
import           Lang.GIR.Transform.EmitGo
import           Lang.GIR.Transform.EraseEmptyFns
import           Lang.GIR.Transform.EraseLets
import           Lang.GIR.Transform.EraseUnit
import           Lang.GIR.Transform.EraseUnusedBindings
import           Lang.GIR.Transform.EraseUnusedExprs
import           Lang.GIR.Transform.InlineGenerics
import           Lang.GIR.Transform.LiftAppValues
import           Lang.GIR.Transform.LiftLets
import           Lang.GIR.Transform.ScopeNames
import           Lang.GIR.Transform.Specialize
import           Lang.GIR.Transform.Transformer
import           Lang.GIR.Transform.TypeAST
import           Lang.GIR.Transform.TypeGlobals
import           Text.Megaparsec (runParserT, parseErrorPretty)


printError :: (MonadIO m) => String -> m ()
printError err = do
  liftIO $ putStrLn $+ err

assertSuccess :: (MonadIO m, MonadError String m, Stringable a) => m (Either a b) -> m b
assertSuccess action = do res <- action
                          case res of
                           Left err -> do printError $+ err
                                          throwError $+ err
                           Right res -> return res

printing :: (Pretty a, MonadIO m) => String -> m a -> m a
printing msg a = do
  res <- a
  liftIO $ putStrLn (msg ++ ":\n")
  pprint res
  return res

process :: (MonadIO m) => m (Either String Package)
process = runExceptT $ do
  pkg <- join $ runParserT package "./testsources/ir/test.gir" <$> (liftIO $ readFile "./testsources/ir/test.gir")
  case pkg of
   Left err -> throwError (parseErrorPretty err)
   Right pkg -> do
     pprint pkg
     pkg <- assertSuccess $ runTypeGlobals pkg
     pkg <- assertSuccess $ runScoper pkg
     pkg <- assertSuccess $ runTyper pkg
     -- pkg <- printing "InlineGenerics" $ assertSuccess $ runInlineGenerics pkg
     pkg <- printing "Specialize"    $ assertSuccess $ runSpecialize pkg
     -- pkg <- printing "LiftAppValues" $ assertSuccess $ runLiftAppValues pkg
     pkg <- printing "LiftLets"      $ assertSuccess $ runLiftLets pkg
     pkg <- printing "EraseEmptyFns" $ assertSuccess $ runEraseEmptyFns pkg
     pkg <- printing "EraseUnusedExprs" $ assertSuccess $ runEraseUnusedExprs pkg
     pkg <- printing "EraseUnusedBindings" $ assertSuccess $ runEraseUnusedBindings pkg
     pkg <- printing "EraseUnit"     $ assertSuccess $ runEraseUnit pkg
     pkg <- printing "EraseLets"     $ assertSuccess $ runEraseLets pkg
     return $ pkg

runGIRTests :: IO ()
runGIRTests = void $ runExceptT $ do
  pkg <- assertSuccess $ process
  pkg <- assertSuccess $ runTyper pkg
  goStr <- assertSuccess $ runEmitGo pkg
  liftIO $ putStrLn "Done!\n"
  liftIO $ putStrLn goStr
  where
    upcaseTest pkg = do
      let upcaseM :: (TransM () m) => String -> m (TResult String)
          upcaseM = return . TRUpdate . map toUpper
          transformers = noopTransformers { tnfTransformString = upcaseM }
      res <- transformPackage transformers pkg ()
      case res of
       Left err -> error err
       Right res -> pprint res

testLetParse :: IO ()
testLetParse = do
  let
    input = "let x = let y = 10 in y in void(let z = 42 in *(x,z))"
    -- input = "let x = let y = 10 in y in x"
  res <- runParserT letExpr "" input
  either (putStrLn . parseErrorPretty) pprint res
