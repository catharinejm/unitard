{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Main where

import Control.Monad.Except
-- import qualified Data.Map.Strict as M
import Data.String.Util
-- import           Lang.GoML.AST
-- import           Lang.GoML.Emitter
-- import           Lang.GoML.Env
import Lang.GoML.Interregnum
-- import Lang.GoML.Passes.Monotize
import Lang.GoML.Passes.Renamer
import Lang.GoML.Passes.InlineGenerics
import Lang.GoML.Parser
import Lang.GoML.Typecheck
-- import           System.IO

assertSuccess :: (MonadIO m, Stringable a) => m (Either a b) -> m b
assertSuccess action = do res <- action
                          case res of
                           Left err -> error $ tos err
                           Right res -> return res

dorun :: IO IRPackage
dorun = do
  source <- readFile "./testsources/main.goml"
  package <- assertSuccess (parseSource source)
  env <- assertSuccess (runExceptT (buildEnv package))
  typedEnv <- assertSuccess (runTyper env)
  irPkg <- assertSuccess (buildIRPackage typedEnv)
  -- inlinedPkg <- assertSuccess (runInliner irPkg)
  return irPkg
  -- mungedPkg <- assertSuccess (runMunger irPkg)
  -- assertSuccess (runMonotize mungedPkg)

main :: IO ()
main = do
  monoPkg <- dorun
  putStrLn $ show monoPkg
  -- putStrLn $+ "envExprTypes:" : map (\(id, typ) -> show id ++ " -> " ++ tos typ) (M.toList (envExprTypes typedEnv))
  -- putStrLn $+ "functionDecls:" : map show (M.elems (functionDecls typedEnv))
  -- goSource <- assertSuccess (runEmitter typedEnv)
  -- putStrLn goSource
