{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Tests where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.Foldable
import qualified Data.Map.Strict as M
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Env
import           Lang.GoML.Parser
import           Lang.GoML.Typecheck
import           Text.Parsec (ParseError)

mkAst :: (MonadIO m, MonadError ParseError m) => m Package
mkAst = do
  pkg <- parseFile "./testsources/test.goml"
  case pkg of
   Left err -> throwError err
   Right pkg -> return pkg

data TestFailure = -- UnexpectedVar BaseType BaseType
                 -- | 
                   Mismatch BaseType BaseType BaseType (Maybe BaseType)
                 | ShouldHaveBeenError Expr
                 | WrongError String String Expr
                 deriving (Show)

instance Stringable TestFailure where
  -- tos (UnexpectedVar expected actual) =
  --   tos [ toBuilder "Unexpected TypeVar in actual type"
  --       , "\texpected:" +|+ expected
  --       , "\tactual:" +|+ actual
  --       ]
  tos (Mismatch expected actual expVar actVar) =
    tos [ "Mismatched Types:" +|+ (maybe (TypeParam "<unknown>") id actVar) +|+ "is not" +|+ expVar
        , "\texpected:" +|+ S expected
        , "\tactual:" +|+ S actual
        ]
  tos (ShouldHaveBeenError expr) =
    tos [ toBuilder "expected typecheck to fail, but it didnt"
        , "\texpr:" +|+ S expr
        ]
  tos (WrongError expected actual expr) =
    tos [ "expected error: `" +:+ expected +:+ "', but got: `" +:+ actual
        , "\texpr:" +|+ S expr
        ]

data MatchState = MS { varMap :: M.Map String Int
                     , expected :: BaseType
                     , actual :: BaseType
                     }

assertType :: forall m. (MonadIO m, MonadReader Env m) => String -> String -> m TestResult
assertType expr typ = do
  expr <- parseExpr expr
  case expr of
   Left err -> return $ Error $ show err
   Right expr -> do
     actual <- evalStateT (runExceptT (preType expr >>= typeof)) emptyState
     case actual of
      Left err -> return $ Error err
      Right actual -> do
        expected <- runParse typeSchemaP typ
        case expected of
         Left err -> return $ Error $ show err
         Right (TypeSchema _ expected, _) -> do
           Right expected <- evalStateT (runExceptT (chaseType expected)) emptyState
           res <- matchTypes expected actual
           case res of
            Left err -> return $ Failure err
            Right _ -> return Success
  where
    matchTypes :: BaseType -> BaseType -> m (Either TestFailure ())
    matchTypes expected actual = evalStateT (runExceptT (printMatch expected actual)) (MS M.empty expected actual)
    printMatch l r = liftIO (putStrLn $+ "matching" +|+ l +|+ "and" +|+ r) >> match' l r
    match' :: (MonadIO m0, MonadState MatchState m0, MonadError TestFailure m0)
              => BaseType -> BaseType -> m0 ()
    -- match' _ (TypeVar _) = join $ throwError <$> (UnexpectedVar <$> gets expected <*> gets actual)
    match' (TypeParam p) (TypeVar n) = do
      existing <- gets $ (M.!? p) . varMap
      case existing of
       Nothing -> modify $ \s -> s { varMap = M.insert p n (varMap s) }
       Just m -> when (n /= m) $ do
         ntyp <- gets $ fmap (TypeParam . fst) . find ((==n) . snd) . M.toList . varMap
         mkMismatch <- Mismatch <$> gets expected <*> gets actual
         throwError $ mkMismatch (TypeParam p) ntyp
    match' (FunType (Sig lhsTypes lhsRet)) (FunType (Sig rhsTypes rhsRet)) = do
      mapM_ (uncurry match') (lhsTypes `zip` rhsTypes)
      match' lhsRet rhsRet
    match' (NamedType n argsLHS) (NamedType ((==n) -> True) argsRHS) = do
      mapM_ (uncurry match') (argsLHS `zip` argsRHS)
    match' lhs rhs = when (lhs /= rhs) $
                     Mismatch <$> gets expected <*> gets actual <*> pure lhs <*> pure (Just rhs) >>= throwError

data TestResult = Success
                | Error String
                | Failure TestFailure
                deriving (Show)

instance Stringable TestResult where
  tos Success = "Success"
  tos (Error msg) = tos $ "ERROR:" +|+ msg
  tos (Failure failure) = tos $ "FAILED:" +|+ failure

runTestCase :: String -> String -> IO TestResult
runTestCase expr tString = do
  astRes <- runExceptT mkAst
  case astRes of
   Left err -> return $ Error (show err)
   Right ast -> do
     env <- runExceptT (buildEnv ast)
     case env of
      Left err -> return (Error err)
      Right env -> runReaderT (assertType expr tString) env

runErrorTestCase :: String -> String -> IO TestResult
runErrorTestCase expr expectedMsg = do
  astRes <- runExceptT mkAst
  case astRes of
   Left err -> return $ Error $ show err
   Right ast -> do
     env <- runExceptT (buildEnv ast)
     case env of
      Left err -> return (Error err)
      Right env -> do
        expr <- parseExpr expr
        case expr of
         Left err -> return $ Error $ show err
         Right expr -> do
           (res, _) <- runExprTyper (preType expr >>= typeof) env
           case res of
            Left err -> if err == expectedMsg
                        then return Success
                        else return $ Failure $ WrongError expectedMsg err expr
            Right _ -> return $ Failure $ ShouldHaveBeenError expr

validTestCases :: [(String, String)]
validTestCases = [ ("(+)", "forall a. a -> a -> a")
                 , ("(+) 5 10", "int")
                 , ("\\f -> f 1", "forall a. (int -> a) -> a")
                 , ("five", "int")
                 , ("1 + 2 + (3 + 4) + 5", "int")
                 , ("5 - -2", "int")
                 , ("let f = FooBarStruct 10 \"hi\" in f.foo", "int")
                 , ("let f = FooBarStruct 10 \"hi\" in f.bar", "string")
                 , ("let f = FooBarStruct 10 \"hi\" in let b = Box f in b", "Box FooBarStruct")
                 , ("let f = FooBarStruct 10 \"hi\" in let b = Box f in b.unBox", "FooBarStruct")
                 , ("let f = FooBarStruct 10 \"hi\" in let b = Box f in b.unBox.bar", "string")
                 , ("let b = \\n -> Box (n+1) in (doBox (\\x -> Box (b x)) 10).unBox.unBox", "int")
                 , ("let b = \\n -> Box (n+1) in let b2 = \\x -> Box (b x) in (b2 10).unBox.unBox", "int")
                 ]

invalidTestCases :: [(String, String)]
invalidTestCases = [ ("let g = \\f -> f 1 2 in let z = g \\x y -> 1 in g \\x y -> \"hi\"",
                      "binding g contains a polymorphic type")
                   , ("\\x -> x x", "unification cycle detected!")
                   , ("1-2", "Type Error: int is not (int -> ?)")
                   ]

runAllTests :: IO ()
runAllTests = do
  validRes <- mapM (uncurry runTestCase) validTestCases
  invalidRes <- mapM (uncurry runErrorTestCase) invalidTestCases
  let res = validRes ++ invalidRes
  mapM_ printResults res
  printCounts res
  where
    printResults (Error err) = putStrLn $+ err
    printResults (Failure f) = putStrLn $+ f
    printResults _ = return ()
    isError (Error _) = True
    isError _ = False
    isFailure (Failure _) = True
    isFailure _ = False
    printCounts res = do
      let runs = length res
          errors = length (filter isError res)
          fails = length (filter isFailure res)
          passes = runs - errors - fails
      putStrLn $+ "Total runs:" +|+ runs +:+ ", Passes:" +|+ passes +:+ ", Errors:" +|+ errors +:+ ", Failures:" +|+ fails
