module Data.String.Util where

import Data.Monoid
import Text.Parsec (ParseError)

data ShowBox = forall a. Show a => S a
instance Show ShowBox where
  show (S a) = show a

infixr 0 $+
($+) :: (Stringable b) => (String -> a) -> b -> a
f $+ b = f $ tos b

newtype StringBuilder = SB [String]
instance Monoid StringBuilder where
  mempty = SB []
  mappend (SB lhs) (SB rhs) = SB (lhs ++ rhs)

infixr 5 +:+
(+:+) :: (Stringable a, Stringable b) => a -> b -> StringBuilder
l +:+ r = toBuilder l <> toBuilder r

infixr 5 +|+
(+|+) :: (Stringable a, Stringable b) => a -> b -> StringBuilder
l +|+ r = l +:+ " " +:+ r

class Stringable a where
  tos :: a -> String
  toBuilder :: a -> StringBuilder
  toBuilder = SB . (:[]) . tos

instance {-# OVERLAPPING #-} Stringable String where
  tos = id

instance Stringable StringBuilder where
  tos (SB strs) = concat strs
  toBuilder = id

instance Stringable ShowBox where
  tos = show

instance Stringable Int where
  tos = show

instance Stringable a => Stringable [a] where
  tos = unlines . map tos

instance Stringable ParseError where
  tos = show
