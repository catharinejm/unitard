module Lang.GIR.Transform.LiftLets where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

type LiftM m = TransM LLState m

data LLState = LLState
             deriving (Show, Eq)

emptyLLState :: LLState
emptyLLState = LLState

runLiftLets :: (MonadIO m) => Package -> m (Either String Package)
runLiftLets package = transformPackage liftLetsTrans package emptyLLState
  where
    liftLetsTrans = noopTransformers { tnfTransformExpr = liftExpr
                                     }

liftExpr :: forall m. (LiftM m) => Expr -> m (TResult Expr)
liftExpr (Let binds sid body typ) = do
  binds <- transform binds
  body <- transform body
  let (liftedLets, newBinds) = unzip $ map liftBind binds
  if any isJust liftedLets
    then do res <- transform $ foldr mergeLets (Let newBinds sid body typ) liftedLets
            return (TRUpdate res)
    else return . TRUpdate $ Let binds sid body typ
  where
    mergeLets Nothing ~rest = rest
    mergeLets (Just (binds, sid)) ~rest = Let binds sid rest typ
    liftBind (lvalue, Let binds sid innerBody _) =
      (Just (binds, sid), (lvalue, innerBody))
    liftBind (lvalue, expr) =
      (Nothing, (lvalue, expr))
liftExpr (App fn params typ) = do
  fn <- transform fn
  params <- transform params
  (liftedLets, (fn : params)) <- unzip <$> mapM liftValue (fn : params)
  if any isJust liftedLets
    then do res <- transform $ foldr mergeLets (App fn params typ) liftedLets
            return (TRUpdate res)
    else return . TRUpdate $ App fn params typ
  where
    liftValue (Let binds sid body typ) = do
      newSID <- ScopeID <$> nextId
      let newLValue = LValue "letResult" typ
          newLocalName = LocalName "letResult" newSID typ
      return $ (Just (binds, sid, [(newLValue, body)], newSID), newLocalName)
    liftValue expr = return (Nothing, expr)
    mergeLets :: Maybe ([(LValue, Expr)], ScopeID, [(LValue, Expr)], ScopeID) -> Expr -> Expr
    mergeLets Nothing ~rest = rest
    mergeLets (Just (binds, sid, newBinds, newSID)) ~rest =
      Let binds sid (Let newBinds newSID rest typ) typ
liftExpr _ = return TRPassThru
