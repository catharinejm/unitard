module Lang.GIR.Transform.EraseUnusedExprs where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.Either
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import qualified Data.Set as S
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

type EraseM m = TransM EUState m

data EUState = EUState { euShouldErase :: [Bool] }
             deriving (Show, Eq)

emptyEUState :: EUState
emptyEUState = EUState []

runEraseUnusedExprs :: (MonadIO m) => Package -> m (Either String Package)
runEraseUnusedExprs package = transformPackage eraseUnusedTrans package emptyEUState
  where
    eraseUnusedTrans = noopTransformers { tnfTransformExpr = eraseUnusedExpr
                                        , tnfTransformCodeSeg = eraseUnusedCodeSeg
                                        }

bracketErasure :: (EraseM m) => Bool -> m a -> m a
bracketErasure b = bracket pushErase popErase
  where
    pushErase = modifyU $ \u -> u { euShouldErase = b : euShouldErase u }
    popErase = modifyU $ \u -> u { euShouldErase = tail (euShouldErase u) }

withErasure :: (EraseM m) => m a -> m a
withErasure = bracketErasure True

withoutErasure :: (EraseM m) => m a -> m a
withoutErasure = bracketErasure False

shouldErase :: (EraseM m) => m Bool
shouldErase = getsU (fromMaybe False . listToMaybe . euShouldErase)

eraseUnusedExpr :: (EraseM m) => Expr -> m (TResult Expr)
eraseUnusedExpr (Fn params UnitType sid body fnType) = do
  body <- withNewScope (paramsScope sid params) $ withErasure $ transform body
  return . TRUpdate $ Fn params UnitType sid body fnType
eraseUnusedExpr fn@(Fn _ _ _ _ _) = TRUpdate <$> withoutErasure (defaultTransform fn)
eraseUnusedExpr (Let binds sid body typ) = do
  binds <- withoutErasure $ transform binds
  body <- withNewScope (letBindScope sid binds) $ transform body
  return . TRUpdate $ Let binds sid body typ
eraseUnusedExpr (Block seg UnitType) = return TRPassThru
eraseUnusedExpr (App fn params appType) = do
  erase <- shouldErase
  (fn : params) <- withoutErasure $ transform (fn : params)
  if erase
    then do sid <- currentScopeID
            let seg = CodeSeg sid [NakedEx (App fn params appType), NakedEx unitExpr] UnitType
                block = Block seg UnitType
            return $ TRUpdate block
    else return $ TRUpdate (App fn params appType)

eraseUnusedExpr _ = do
  erase <- shouldErase
  if erase
    then return $ TRUpdate unitExpr
    else return TRPassThru

eraseUnusedCodeSeg :: (EraseM m) => CodeSeg -> m (TResult CodeSeg)
eraseUnusedCodeSeg (CodeSeg _ [] _) = return TRCut
eraseUnusedCodeSeg (CodeSeg sid stmts typ) = do
  initStmts <- withExistingScope sid $ withErasure $ transform (init stmts)
  lastStmt <- withExistingScope sid $ transform (last stmts)
  return . TRUpdate $ CodeSeg sid (initStmts ++ [lastStmt]) typ
