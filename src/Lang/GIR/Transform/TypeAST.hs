module Lang.GIR.Transform.TypeAST where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.IO.Class
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.AST.Util
import           Lang.GIR.Transform.Transformer

data TyState = TyState { tystTypeVars       :: Map Int BaseType
                       , tystVarConstraints :: Map Int [TypeConstraint]
                       }
emptyTyState :: TyState
emptyTyState = TyState M.empty M.empty

type TyperM m = TransM TyState m

runTyper :: (MonadIO m) => Package -> m (Either String Package)
runTyper package = runExceptT $ do
  (env, st) <- join $ assertSuccess <$> runTransform (TEnv typerTrans package) (emptyTState emptyTyState)
  (env, _) <- join $ assertSuccess <$> runTransform (env { teTransformers = eraserTrans }) st
  return $ tePackage env
  where
    typerTrans = noopTransformers { tnfTransformExpr = typeExpr
                                  , tnfTransformStatement = typeStatement
                                  , tnfTransformCodeSeg = typeCodeSeg
                                  }
    eraserTrans = noopTransformers { tnfTransformExpr = eraseExpr, tnfTransformBaseType = eraseTypeVar }
    assertSuccess (Left err) = throwError err
    assertSuccess (Right res) = return res

typeError :: (TyperM m) => BaseType -> BaseType -> m ()
typeError lhs rhs =
  throwError $+ "Type Error:" +|+ lhs +|+ "is not" +|+ rhs

lookupTypeVar :: (TyperM m) => Int -> m (Maybe BaseType)
lookupTypeVar n = getsU ((M.!? n) . tystTypeVars)

chaseType :: (TyperM m) => BaseType -> m BaseType
chaseType tv@(TypeVar n) = join $ maybe (return tv) chaseType <$> lookupTypeVar n
chaseType (TypeName name tparams) = TypeName name <$> mapM chaseType tparams
chaseType tp@(TypeParam _) = return tp
chaseType (FnType paramTypes retType) = FnType <$> mapM chaseType paramTypes <*> chaseType retType
chaseType p@(Prim _) = return p
chaseType UnitType = return UnitType
chaseType (StructType fields) = StructType <$> mapM chaseField fields
  where
    chaseField (LValue name typ) = LValue name <$> chaseType typ

varCycleCheck :: (TyperM m) => Int -> BaseType -> m ()
varCycleCheck idx typ = do
  chased <- chaseType typ
  case chased of
   FnType typs ret -> do
     mapM_ (varCycleCheck idx) typs
     varCycleCheck idx ret
   TypeVar n -> when (n == idx) $ throwError "unification cycle detected!"
   _ -> return ()

setTypeVar :: (TyperM m) => Int -> BaseType -> m ()
setTypeVar idx typ = do
  typ <- chaseType typ
  varCycleCheck idx typ
  modifyU $ \u -> u { tystTypeVars = M.insert idx typ (tystTypeVars u) }

applyType :: (TyperM m) => TypeDef -> [BaseType] -> m BaseType
applyType (TypeDef _ tvs typ) targs = join $ subst tvs <$> mapM chaseType targs <*> chaseType typ

lookupStructuralType :: (TyperM m) => String -> [BaseType] -> m BaseType
lookupStructuralType tname targs = do
  tdef <- lookupTypeDef tname
  applyType tdef targs

unifyStructuralTypes :: (TyperM m) => String -> [BaseType] -> BaseType -> m ()
unifyStructuralTypes tname targs rhs = do
  lhs <- lookupStructuralType tname targs
  unifyTypes lhs rhs

structuralTypeOf :: (TyperM m) => BaseType -> m BaseType
structuralTypeOf (TypeName name targs) = do
  lookupStructuralType name targs
structuralTypeOf typ = return typ

assertTypeEq :: (TyperM m) => BaseType -> BaseType -> m ()
assertTypeEq lhs rhs = do
  lhs <- chaseType lhs
  rhs <- chaseType rhs
  when (lhs /= rhs) $ typeError lhs rhs

unifyTypes :: (TyperM m) => BaseType -> BaseType -> m ()
unifyTypes lhs rhs = do
  lhs <- chaseType lhs
  rhs <- chaseType rhs
  -- liftIO $ putStrLn $+ "unifyTypes" +|+ lhs +|+ rhs
  unify' lhs rhs
  where
    unify' (TypeVar i) rhs = setTypeVar i rhs
    unify' lhs (TypeVar i) = setTypeVar i lhs
    unify' lhs@(TypeName _ _) rhs@(TypeName _ _) = assertTypeEq lhs rhs
    unify' (TypeName name targs) rhs = unifyStructuralTypes name targs rhs
    unify' lhs (TypeName name targs) = unifyStructuralTypes name targs lhs
    unify' lhs@(FnType largs lret) rhs@(FnType rargs rret) = do
      when (length largs /= length rargs) $ typeError lhs rhs
      mapM_ (uncurry unifyTypes) $ largs `zip` rargs
      unifyTypes lret rret
    unify' lhs rhs = assertTypeEq lhs rhs

lookupConstraints :: (TyperM m) => Int -> m [TypeConstraint]
lookupConstraints n = do
  fromMaybe [] <$> getsU ((M.!? n) . tystVarConstraints)

addConstraint :: (TyperM m) => BaseType -> TypeConstraint -> m ()
addConstraint typ newConstraint = join $ addC <$> chaseType typ
  where
    addC (TypeVar n) = do
      constraints <- lookupConstraints n
      constraints <- reconcileConstraints newConstraint constraints
      modifyU $ \u -> u { tystVarConstraints = M.insert n constraints (tystVarConstraints u) }
    addC typ = applyConstraint newConstraint typ

reconcileConstraints :: (TyperM m) => TypeConstraint -> [TypeConstraint] -> m [TypeConstraint]
reconcileConstraints new@(HasField fname ftype) existing = do
  case find fieldMatch existing of
   Nothing -> return (new:existing)
   Just (HasField _ otherTyp) -> do unifyTypes ftype otherTyp
                                    return existing
  where
    fieldMatch (HasField f _) = f == fname

applyConstraint :: (TyperM m) => TypeConstraint -> BaseType -> m ()
applyConstraint cns typ = join $ apply' cns <$> chaseType typ
  where
    apply' cns tv@(TypeVar _) = addConstraint tv cns
    apply' (HasField fname ftype) typ = do
      structuralType <- structuralTypeOf typ
      case structuralType of
       StructType fields ->
         case find ((==fname) . nameOf) fields of
          Nothing -> noField
          Just (LValue _ rhsType) -> unifyTypes ftype rhsType
       _ -> noField
      where
        noField = throwError $+ "type" +|+ typ +|+ "does not have field" +|+ fname

typeExpr :: (TyperM m) => Expr -> m (TResult Expr)
typeExpr expr = do
  -- liftIO $ putStrLn $+ "typeExpr" +|+ expr
  res <- doType expr `catchError` \err ->
    throwError $+ "Error processing expression:\n" +:+ expr +:+ "\n" +:+ err
  case res of
   TRUpdate expr -> do
     -- liftIO $ putStrLn $+ "resulting type:" +|+ underlyingType expr
     return ()
   _ -> return ()
  return res
  where
    doType (LocalName name sid typ) = do
      bind <- lookupBinding sid name
      unifyTypes (underlyingType bind) typ
      TRUpdate . LocalName name sid <$> chaseType typ
    doType (GlobalName name targs typ) = do
      (TypeSchema tvs definedType) <- typeSchema <$> lookupDef name
      concDefinedType <- join $ subst tvs <$> mapM chaseType targs <*> pure definedType
      -- liftIO $ putStrLn $+ "concDefinedType:" +|+ concDefinedType
      unifyTypes concDefinedType typ
      TRUpdate . GlobalName name targs <$> chaseType typ
    doType (App fn params typ) = do
      fn <- transform fn
      params <- transform params
      let fnType = FnType (map underlyingType params) typ
      unifyTypes (underlyingType fn) fnType
      TRUpdate . App fn params <$> chaseType typ
    doType (Prop recv field typ) = do
      recv <- transform recv
      recvVar <- newTypeVar
      fieldValueVar <- newTypeVar
      addConstraint recvVar (HasField field fieldValueVar)
      unifyTypes recvVar (underlyingType recv)
      unifyTypes typ fieldValueVar
      TRUpdate . Prop recv field <$> chaseType typ
    doType (Fn params retType sid body fnType) = do
      params <- transform params
      body <- withNewScope (paramsScope sid params) $ transform body
      -- liftIO $ putStrLn "UNIFY FN BODY"
      -- liftIO $ putStrLn $+ "retType:" +|+ retType
      -- liftIO $ putStrLn $+ "bodyType:" +|+ bodyType
      unifyTypes (underlyingType body) retType
      retType <- chaseType retType
      let fnTypeShape = FnType (map underlyingType params) retType
      unifyTypes fnType fnTypeShape
      TRUpdate . Fn params retType sid body <$> chaseType fnType
    doType (Let binds sid body typ) = do
      binds <- transform binds
      mapM_ (uncurry unifyTypes . (underlyingType *** underlyingType)) binds
      body <- withNewScope (letBindScope sid binds) $ transform body
      unifyTypes (underlyingType body) typ
      TRUpdate . Let binds sid body <$> chaseType typ
    doType (Block seg typ) = do
      seg <- transform seg
      unifyTypes (underlyingType seg) typ
      TRUpdate . Block seg <$> chaseType typ
    doType (Lit lit typ) = do
      unifyTypes (litType lit) typ
      TRUpdate . Lit lit <$> chaseType typ

typeStatement :: (TyperM m) => Statement -> m (TResult Statement)
typeStatement (NakedEx _) = return TRPassThru
typeStatement (InitVar name typ sid expr) = do
  expr <- transform expr
  unifyTypes typ (underlyingType expr)
  initVar name typ sid expr
  return . TRUpdate $ InitVar name typ sid expr
typeStatement (SetVar name sid expr) = do
  expr <- transform expr
  bind <- lookupBinding sid name
  setVar name sid expr
  unifyTypes (underlyingType bind) (underlyingType expr)
  return . TRUpdate $ SetVar name sid expr

typeCodeSeg :: (TyperM m) => CodeSeg -> m (TResult CodeSeg)
typeCodeSeg seg@(CodeSeg sid stmts typ) = do
  stmts <- withExistingScope sid $ transform stmts
  -- mapM_ (uncurry unifyTypes) $ map underlyingType (sideEffectingStatements seg) `zip` repeat UnitType
  unifyTypes (underlyingType $ returnValue seg) typ
  TRUpdate . CodeSeg sid stmts <$> chaseType typ

eraseExpr :: (TyperM m) => Expr -> m (TResult Expr)
eraseExpr expr = do
  -- liftIO $ putStrLn $+ "typeExpr" +|+ expr
  res <- defaultTransform expr `catchError` \err -> 
    throwError $+ "Error processing expression:\n" +:+ expr +:+ "\n" +:+ err
  return $ TRUpdate res

eraseTypeVar :: (TyperM m) => BaseType -> m (TResult BaseType)
eraseTypeVar typ = do
  res <- chaseType typ
  if isConcrete res
    then return $ TRUpdate res
    else throwError $+ "found unbound type var after type checking:" +|+ typ
