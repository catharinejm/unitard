module Lang.GIR.Transform.TypeGlobals where

import Control.Monad.Except
import Control.Monad.IO.Class
import Data.String.Util
import Lang.GIR.AST
import Lang.GIR.Transform.Transformer

type TGlobM m = TransM TGState m

data TGState = TGState
emptyTGState :: TGState
emptyTGState = TGState

runTypeGlobals :: (MonadIO m) => Package -> m (Either String Package)
runTypeGlobals package = transformPackage typeGlobalsTrans package emptyTGState
  where
    typeGlobalsTrans = cutTransformers { tnfTransformDef = typeGlobalDef
                                       }

trivialType :: Expr -> BaseType
trivialType (Fn params retType _ _ _) = FnType (map underlyingType params) retType
trivialType e = underlyingType e

typeGlobalDef :: (TGlobM m) => Def -> m (TResult Def)
typeGlobalDef (Def name tparams tv@(TypeVar _) body) = do
  let bodyType = maybe tv trivialType body
  -- liftIO $ putStrLn $ show $ fmap tos body
  unless (isConcrete bodyType) $
    throwError $+ "Cannot infer top-level type for" +|+ name +:+ ". Please provide an annotation"
  return . TRUpdate $ Def name tparams bodyType body
typeGlobalDef _ = return TRCut
