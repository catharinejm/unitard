module Lang.GIR.Transform.InlineGenerics where

import           Control.Monad.Except
import           Control.Monad.IO.Class
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

data IGState = IGState { igInlines         :: Map (String, [BaseType]) ScopeID
                       , igScopeMap        :: Map ScopeID ScopeID
                       , igSpecializations :: Map (String, [BaseType]) (String, ScopeID, BaseType)
                       }
emptyIGState :: IGState
emptyIGState = IGState M.empty M.empty M.empty

type InlineM m = TransM IGState m

runInlineGenerics :: (MonadIO m) => Package -> m (Either String Package)
runInlineGenerics package = transformPackage inlinerTrans package emptyIGState
  where
    inlinerTrans = noopTransformers { tnfTransformDef = inlineDef
                                    , tnfTransformExpr = inlineExpr
                                    }

inlineDef :: (InlineM m) => Def -> m (TResult Def)
inlineDef (Def _ (_:_) _ (Just _)) = return TRCut
inlineDef _ = return TRPassThru

data Backtrack = Backtrack ScopeID
               deriving (Show, Eq)

withInline :: (InlineM m) => String -> [BaseType] -> ScopeID -> (ScopeID -> m a) -> m a
withInline name targs fnSID action = do
  -- liftIO $ putStrLn $+ "withInline" +|+ name +|+ fnSID
  sid <- ScopeID <$> nextId
  bracket (pushInline sid) popInline (action sid)
  where
    pushInline sid = do
      -- liftIO $ putStrLn "pushInline"
      modifyU $ \u -> u { igInlines = M.insert (name, targs) sid (igInlines u)
                        , igScopeMap = M.insert fnSID sid (igScopeMap u)
                        }
    popInline = do
      -- liftIO $ putStrLn "popInline"
      modifyU $ \u -> u { igInlines = M.delete (name, targs) (igInlines u)
                        , igScopeMap = M.delete fnSID (igScopeMap u)
                        }

withSpecialization :: (InlineM m) => (String, [BaseType]) -> (String, ScopeID, BaseType) -> m a -> m a
withSpecialization globInfo localInfo = bracket pushSpecial popSpecial
  where
    pushSpecial = modifyU $ \u -> u { igSpecializations = M.insert globInfo localInfo (igSpecializations u) }
    popSpecial = modifyU $ \u -> u { igSpecializations = M.delete globInfo (igSpecializations u) }

inlineScopeID :: (InlineM m) => String -> [BaseType] -> m (Maybe ScopeID)
inlineScopeID name targs = do
  -- liftIO $ putStrLn $+ "inlineScopeID" +|+ name
  inlineStack <- getsU igInlines
  -- liftIO $ putStrLn $+ "igInlines:" +|+ S inlineStack
  getsU ((M.!? (name, targs)) . igInlines)

bACKTRACK :: String
bACKTRACK = "\8\18\20\2\10"

inlineExpr :: (InlineM m) => Expr -> m (TResult Expr)
inlineExpr (App fn args appType) = do
  fn <- transform fn
  case fn of
   GlobalName name targs@(_:_) _ -> do
     -- liftIO $ putStrLn $+ "inlining" +|+ name
     activeSID <- inlineScopeID name targs
     -- liftIO $ putStrLn $+ "activeSID" +|+ S activeSID
     case activeSID of
      Nothing -> inline name targs args appType
      Just _ -> throwError bACKTRACK
   nonGeneric -> TRUpdate <$> (App nonGeneric <$> transform args <*> pure appType)
inlineExpr (LocalName name sid typ) = do
  newSID <- chaseScope sid
  return . TRUpdate $ LocalName name newSID typ
inlineExpr (GlobalName name targs _) = do
  special <- getsU ((M.!? (name, targs)) . igSpecializations)
  case special of
   Nothing -> return TRPassThru
   Just (lname, sid, typ) -> return . TRUpdate $ LocalName name sid typ 
inlineExpr _ = return TRPassThru

chaseScope :: (InlineM m) => ScopeID -> m ScopeID
chaseScope sid = do
  msid <- getsU ((M.!? sid) . igScopeMap)
  maybe (return sid) chaseScope msid

inline :: (InlineM m) => String -> [BaseType] -> [Expr] -> BaseType -> m (TResult Expr)
inline name targs args appType = do
  def@(Def _ tvs _ body) <- lookupDef name
  body <- maybe (noBody def) return body
  substFn@(Fn params retType fnSID body fnType) <- subst tvs targs body
  let retry err = do
        when (err /= bACKTRACK) (throwError err)
        specialize name targs args appType substFn
  flip catchError retry $ withInline name targs fnSID $ \sid -> do
    args <- transform args
    let binds = params `zip` args
    body <- withNewScope (letBindScope sid binds) $ transform body
    return . TRUpdate $ Let binds sid body retType
  where
    noBody def = throwError $+ "Generic defintion has no body:" +|+ def

specialize :: (InlineM m) => String -> [BaseType] -> [Expr] -> BaseType -> Expr -> m (TResult Expr)
specialize name targs args appType substFn = do
  let Fn params retType fnSID body fnType = substFn
  sid <- ScopeID <$> nextId
  spcName <- ((name ++ "_specialized") ++) . show <$> nextId
  let lval = LValue spcName fnType
      binds = [Var lval Nothing]
      globInfo = (name, targs)
      localInfo = (spcName, sid, fnType)
  specialFn <- withNewScope (Scope sid binds) $
               withSpecialization globInfo localInfo $
               transform substFn
  newApp <- App (LocalName spcName sid fnType) <$> transform args <*> pure appType
  return . TRUpdate $ Let [(LValue spcName fnType, specialFn)] sid newApp appType
