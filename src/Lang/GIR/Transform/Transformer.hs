module Lang.GIR.Transform.Transformer where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GIR.AST

type (TransM u m) = (MonadIO m, MonadError String m, MonadState (TState u) m, MonadReader (TEnv u) m)

data TState u = TState { tsScopePath :: [Scope]
                       , tsAllScopes :: M.Map ScopeID Scope
                       , tsUserState :: u
                       }
              deriving (Show)

emptyTState :: u -> TState u
emptyTState = TState [] M.empty

data TEnv u = TEnv { teTransformers :: Transformers u
                   , tePackage      :: Package
                   }

instance Show (TEnv u) where
  show (TEnv _ pkg) = "TEnv {teTransformers = <...>, tePackage = " ++ show pkg ++ "}"

type TAction m a = a -> m (TResult a)
data TResult a = TRUpdate a
               | TRPassThru
               | TRCut
               deriving (Show, Eq)

data Transformers u = Transformers { tnfTransformTypeDef    :: forall m. (TransM u m) => TAction m TypeDef
                                   , tnfTransformDef        :: forall m. (TransM u m) => TAction m Def
                                   , tnfTransformCodeSeg    :: forall m. (TransM u m) => TAction m CodeSeg
                                   , tnfTransformStatement  :: forall m. (TransM u m) => TAction m Statement
                                   , tnfTransformExpr       :: forall m. (TransM u m) => TAction m Expr
                                   , tnfTransformLValue     :: forall m. (TransM u m) => TAction m LValue
                                   , tnfTransformTypeSchema :: forall m. (TransM u m) => TAction m TypeSchema
                                   , tnfTransformBaseType   :: forall m. (TransM u m) => TAction m BaseType
                                   , tnfTransformScope      :: forall m. (TransM u m) => TAction m Scope
                                   , tnfTransformScopeID    :: forall m. (TransM u m) => TAction m ScopeID
                                   , tnfTransformString     :: forall m. (TransM u m) => TAction m String
                                   }

noopTransformers :: Transformers u
noopTransformers = Transformers { tnfTransformTypeDef    = return . const TRPassThru
                                , tnfTransformDef        = return . const TRPassThru
                                , tnfTransformCodeSeg    = return . const TRPassThru
                                , tnfTransformStatement  = return . const TRPassThru
                                , tnfTransformExpr       = return . const TRPassThru
                                , tnfTransformLValue     = return . const TRPassThru
                                , tnfTransformBaseType   = return . const TRPassThru
                                , tnfTransformTypeSchema = return . const TRPassThru
                                , tnfTransformScope      = return . const TRPassThru
                                , tnfTransformScopeID    = return . const TRPassThru
                                , tnfTransformString     = return . const TRPassThru
                                }

cutTransformers :: Transformers u
cutTransformers = Transformers { tnfTransformTypeDef    = return . const TRCut
                               , tnfTransformDef        = return . const TRCut
                               , tnfTransformCodeSeg    = return . const TRCut
                               , tnfTransformStatement  = return . const TRCut
                               , tnfTransformExpr       = return . const TRCut
                               , tnfTransformLValue     = return . const TRCut
                               , tnfTransformBaseType   = return . const TRCut
                               , tnfTransformTypeSchema = return . const TRCut
                               , tnfTransformScope      = return . const TRCut
                               , tnfTransformScopeID    = return . const TRCut
                               , tnfTransformString     = return . const TRCut
                               }

transformPackage :: (MonadIO m) => Transformers u -> Package -> u -> m (Either String Package)
transformPackage trans pkg userSt =
  either throwError (return . tePackage . fst) <$> runTransform (TEnv trans pkg) (emptyTState userSt)

runTransform :: (MonadIO m) => TEnv u -> TState u -> m (Either String (TEnv u, TState u))
runTransform tenv userSt = do
  (res, tenv') <- runStateT (runExceptT (transformTypeDefs >>= transformDefs)) tenv
  either (return . Left) (return . Right . (\r -> (tenv', r))) res
  where
    transformTypeDefs = do
      te@TEnv{tePackage=Package{..}} <- get
      (newTds, st) <- runTransformFrom te userSt (transform (M.elems packageTypeDefs))
      newTds <- either throwError return newTds
      modify $ \e -> e { tePackage = (tePackage e) { packageTypeDefs = namedMap newTds } }
      return st
    transformDefs st = do
      te@TEnv{tePackage=Package{..}} <- get
      (newDefs, st) <- runTransformFrom te st (transform (M.elems packageDefs))
      newDefs <- either throwError return newDefs
      modify $ \e -> e { tePackage = (tePackage e) { packageDefs = namedMap newDefs } }
      return st

type Trans u m = ExceptT String (StateT (TState u) (ReaderT (TEnv u) m))

runTransformFrom :: (MonadIO m, Transformable a)
                    => TEnv u -> TState u -> Trans u m a -> m (Either String a, TState u)
runTransformFrom trans st action = runReaderT (runStateT (runExceptT action) st) trans

localTransform :: (TransM u m, Transformable a) => Trans v m a -> Transformers v -> v -> m a
localTransform action trans v = do
  env <- ask
  stu <- get
  (res, _) <- runTransformFrom (env { teTransformers = trans }) (stu { tsUserState = v }) action
  either throwError return res

transform :: (Transformable a, TransM u m) => a -> m a
transform x = do
  trans <- asks teTransformers
  mres <- overrideTransformer trans x
  case mres of
   TRUpdate res -> return res
   TRPassThru -> defaultTransform x
   TRCut -> return x

getU :: (TransM u m) => m u
getU = gets tsUserState

getsU :: (TransM u m) => (u -> a) -> m a
getsU f = gets (f . tsUserState)

modifyU :: (TransM u m) => (u -> u) -> m ()
modifyU f = modify $ \s -> s { tsUserState = f (tsUserState s) }

withExistingScope :: (TransM u m) => ScopeID -> m a -> m a
withExistingScope sid action = join $ withNewScope <$> lookupScope sid <*> pure action

bracket :: (TransM u m) => m () -> m () -> m a -> m a
bracket setup teardown action = do
  setup
  res <- action `catchError` teardownAndThrow
  teardown
  return res
  where
    teardownAndThrow err = teardown >> throwError err

withNewScope :: (TransM u m) => Scope -> m a -> m a
withNewScope newScope action = bracket pushScope popScope action
  where
    pushScope = modify $ \s ->
      s { tsScopePath = newScope : tsScopePath s
        , tsAllScopes = M.insert (scopeID newScope) newScope (tsAllScopes s)
        }
    popScope = modify $ \s -> s { tsScopePath = tail (tsScopePath s) }

paramsScope :: ScopeID -> [LValue] -> Scope
paramsScope sid params = Scope sid binds
  where
    binds = map (uncurry FunParam) $ [0..] `zip` params

letBindScope :: ScopeID -> [(LValue, Expr)] -> Scope
letBindScope sid letbinds = Scope sid binds
  where
    binds = map (\(lv, e) -> Var lv $ Just e) letbinds

currentScope :: (TransM u m) => m Scope
currentScope = do
  mscope <- gets (listToMaybe . tsScopePath)
  case mscope of
   Nothing -> throwError "there is no active scope"
   Just scope -> return scope

currentScopeID :: (TransM u m) => m ScopeID
currentScopeID = scopeID <$> currentScope

lookupScope :: (TransM u m) => ScopeID -> m Scope
lookupScope sid = do
  scopes <- gets tsScopePath
  return $ fromMaybe (Scope sid []) (find ((==sid) . scopeID) scopes)

getBinding :: Scope -> String -> Maybe Binding
getBinding scope name = find ((==name) . nameOf) $ scopeBinds scope

getBindingIndex :: Scope -> String -> Maybe Int
getBindingIndex scope name = findIndex ((==name) . nameOf) $ scopeBinds scope

lookupBinding :: (TransM u m) => ScopeID -> String -> m Binding
lookupBinding sid name = do
  scope <- lookupScope sid
  maybe undefinedName return (getBinding scope name)
  where
    undefinedName = throwError $+ "undefined name:" +|+ name +|+ "(scope" +|+ sid +:+ ")"

lookupDef :: (TransM u m) => String -> m Def
lookupDef name = do
  mdef <- asks ((M.!? name) . packageDefs . tePackage)
  maybe undefinedName return mdef
  where
    undefinedName = throwError $+ "undefined name:" +|+ name

lookupTypeDef :: (TransM u m) => String -> m TypeDef
lookupTypeDef name = do
  mdef <- asks ((M.!? name) . packageTypeDefs . tePackage)
  maybe undefinedType return mdef
  where
    undefinedType = throwError $+ "undefined type:" +|+ name

assertTypeArity :: (TransM u m) => [a] -> [b] -> m ()
assertTypeArity expected actual = do
  when (length expected /= length actual) $
    throwError $+ "expected" +|+ length expected +|+ "type params, but got" +|+ length actual

updateScope :: (TransM u m) => Scope -> m ()
updateScope scope = do
  (before, after) <- gets (break ((== scopeID scope) . scopeID) . tsScopePath)
  let newScopes = case after of
                   [] -> scope : before
                   (_:after) -> before ++ scope : after
  modify $ \s -> s { tsScopePath = newScopes }

initVar :: (TransM u m) => String -> BaseType -> ScopeID -> Expr -> m ()
initVar name typ sid value = do
  -- liftIO $ putStrLn $+ "initVar" +|+ name +|+ typ +|+ sid +|+ value
  scope <- lookupScope sid
  -- liftIO $ putStrLn $+ "old scope:" +|+ (scopeBinds scope)
  when (isJust $ getBinding scope name) $
    throwError $+ "name" +|+ name +|+ "is already defined" +|+ (scopeBinds scope)
  let updatedScope = scope { scopeBinds = scopeBinds scope ++ [Var (LValue name typ) (Just value)] }
  updateScope updatedScope
  -- newScope <- gets ((M.! sid) . tsAllScopes)
  -- liftIO $ putStrLn $+ "new scope:" +|+ (scopeBinds newScope)

setVar :: (TransM u m) => String -> ScopeID -> Expr -> m ()
setVar name sid value = do
  scope <- lookupScope sid
  case getBindingIndex scope name of
   Nothing -> throwError $+ "undefined name:" +|+ name +|+ "(SetVar)"
   Just bindIdx -> do
     let (before, (oldBind:after)) = splitAt bindIdx (scopeBinds scope)
         Var lvalue _ = oldBind
         updatedScope = Scope sid (before ++ Var lvalue (Just value) : after)
     updateScope updatedScope

subst :: (TransM u m, Transformable a) => [String] -> [BaseType] -> a -> m a
subst tvs targs x = do
  assertTypeArity tvs targs
  let varMap = M.fromList $ tvs `zip` targs
  localTransform (transform x) substTransformer varMap
  where
    substTransformer = noopTransformers { tnfTransformBaseType = substType }
    substType (TypeParam name) = do
      mt <- gets ((M.!? name) . tsUserState)
      case mt of
       Nothing -> return TRPassThru
       Just typ -> return $ TRUpdate typ
    substType _ = return TRPassThru

rescope :: (TransM u m, Transformable a) => ScopeID -> ScopeID -> a -> m a
rescope oldSID newSID x = do
  localTransform (transform x) rescopeTrans ()
  where
    rescopeTrans = noopTransformers { tnfTransformScopeID = rescopeScopeID }
    rescopeScopeID :: (TransM () m) => ScopeID -> m (TResult ScopeID)
    rescopeScopeID sid = if sid == oldSID
                         then return $ TRUpdate newSID
                         else return TRPassThru

-- lookupFieldType :: (TransM u m) => BaseType -> String -> m BaseType
-- lookupFieldType typ@(StructType fields) field =
--   case find ((==field) . nameOf) fields of
--    Nothing -> throwError $+ "type" +|+ typ +|+ "has no field named `" +:+ field +:+ "'"
--    Just f -> return $ underlyingType f
-- lookupFieldType typ@(TypeName _ name typs) field = do
--   TypeDef _ tvs typ <- lookupTypeDef name
--   join $ lookupFieldType <$> subst tvs typs typ <*> pure field

class Transformable a where
  defaultTransform :: (TransM u m) => a -> m a
  overrideTransformer :: (TransM u m) => Transformers u -> TAction m a
  overrideTransformer _ _ = return TRPassThru

instance {-# OVERLAPPABLE #-} (Transformable a) => Transformable [a] where
  defaultTransform = mapM transform

instance (Transformable a) => Transformable (Maybe a) where
  defaultTransform = mapM transform

instance (Transformable a, Transformable b) => Transformable (a, b) where
  defaultTransform (a, b) = (,) <$> transform a <*> transform b

instance (Transformable k, Ord k, Transformable v) => Transformable (M.Map k v) where
  defaultTransform = fmap M.fromList . transform . M.toList

instance Transformable Expr where
  defaultTransform (LocalName name sid typ) =
    LocalName <$> transform name
              <*> transform sid
              <*> transform typ
  defaultTransform (GlobalName name targs schema) =
    GlobalName <$> transform name
               <*> transform targs
               <*> transform schema
  defaultTransform (App fn args typ) =
    App <$> transform fn
        <*> transform args
        <*> transform typ
  defaultTransform (Prop recv field typ) =
    Prop <$> transform recv
         <*> transform field
         <*> transform typ
  defaultTransform (Fn params retType sid body typ) = do
    params <- transform params
    retType <- transform retType
    sid <- transform sid
    body <- withNewScope (paramsScope sid params) $ transform body
    Fn params retType sid body <$> transform typ
  defaultTransform (Let binds sid body typ) = do
    binds <- transform binds
    sid <- transform sid
    withNewScope (letBindScope sid binds) $ do
      Let binds sid <$> transform body <*> transform typ
  defaultTransform (Block seg typ) =
    Block <$> transform seg <*> transform typ
  defaultTransform (Lit literal typ) =
    Lit literal <$> transform typ

  overrideTransformer = tnfTransformExpr

instance Transformable CodeSeg where
  defaultTransform (CodeSeg sid stmts typ) = do
    sid <- transform sid
    stmts <- withExistingScope sid $ transform stmts
    CodeSeg sid stmts <$> transform typ
  overrideTransformer = tnfTransformCodeSeg

instance Transformable Statement where
  defaultTransform (NakedEx ex) = NakedEx <$> transform ex
  -- defaultTransform (Return sid expr) =
  --   Return <$> transform sid <*> transform expr
  defaultTransform (InitVar name typ sid value) = do
    name <- transform name
    typ <- transform typ
    sid <- transform sid
    value <- transform value
    initVar name typ sid value
    return $ InitVar name typ sid value
  defaultTransform (SetVar name sid value) = do
    name <- transform name
    sid <- transform sid
    value <- transform value
    setVar name sid value
    return $ SetVar name sid value

  overrideTransformer = tnfTransformStatement

instance Transformable Binding where
  defaultTransform (FunParam i lvalue) = FunParam i <$> transform lvalue
  defaultTransform (Var lvalue mexpr) = Var <$> transform lvalue <*> transform mexpr

instance Transformable Scope where
  defaultTransform (Scope sid binds) = Scope <$> transform sid <*> transform binds
  overrideTransformer = tnfTransformScope

instance Transformable ScopeID where
  defaultTransform = return
  overrideTransformer = tnfTransformScopeID

instance Transformable String where
  defaultTransform = return
  overrideTransformer = tnfTransformString

instance Transformable TypeSchema where
  defaultTransform (TypeSchema tvs typ) = TypeSchema <$> transform tvs <*> transform typ
  overrideTransformer = tnfTransformTypeSchema

instance Transformable BaseType where
  defaultTransform (TypeName name targs) =
    TypeName <$> transform name
             <*> transform targs
  defaultTransform (TypeParam name) = TypeParam <$> transform name
  defaultTransform (FnType types typ) =
    FnType <$> transform types
           <*> transform typ
  defaultTransform (Prim p) = Prim <$> transform p
  defaultTransform UnitType = return UnitType
  defaultTransform (StructType fields) = StructType <$> transform fields
  defaultTransform tv@(TypeVar _) = return tv

  overrideTransformer = tnfTransformBaseType

instance Transformable LValue where
  defaultTransform (LValue name typ) = LValue <$> transform name <*> transform typ

  overrideTransformer = tnfTransformLValue

instance Transformable TypeDef where
  defaultTransform (TypeDef name targs typ) =
    TypeDef <$> transform name
            <*> transform targs
            <*> transform typ

  overrideTransformer = tnfTransformTypeDef

instance Transformable Def where
  defaultTransform (Def name targs typ body) =
    Def <$> transform name
        <*> transform targs
        <*> transform typ
        <*> transform body

  overrideTransformer = tnfTransformDef
