module Lang.GIR.Transform.EraseEmptyFns where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.State.Strict
import           Data.Either
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import qualified Data.Set as S
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

type EraseM m = TransM EFState m

data EFState = EFState { efErasedLocals :: S.Set (String, ScopeID) }
             deriving (Show, Eq)

emptyEFState :: EFState
emptyEFState = EFState S.empty

runEraseEmptyFns :: (MonadIO m) => Package -> m (Either String Package)
runEraseEmptyFns package = runExceptT $ do
  (env, st) <- join $ assertSuccess <$> runTransform (TEnv eraseEmptyFnsTrans package) (emptyTState emptyEFState)
  (env, _) <- join $ assertSuccess <$> runTransform (env { teTransformers = eraseDeletedNamesTrans }) st
  let pkg = tePackage env
  dropEmptyGlobals pkg
  where
    eraseEmptyFnsTrans = noopTransformers { tnfTransformExpr = eraseEmptyFnsExpr }
    eraseDeletedNamesTrans = noopTransformers { tnfTransformExpr = eraseEmptyNamesExpr }
    assertSuccess (Left err) = throwError err
    assertSuccess (Right res) = return res
    dropEmptyGlobals (Package name tds defs) = do
      newDefs <- mapM dropEmpty $ M.toList defs
      return $ Package name tds (M.fromList (catMaybes newDefs))
    dropEmpty pair@(_, Def _ _ _ (Just (Fn _ _ _ body _))) = do
      if isEmptyValue body
        then return Nothing
        else return $ Just pair
    dropEmpty pair = return $ Just pair

isEmptyFn :: (EraseM m) => Expr -> m Bool
isEmptyFn (Fn _ _ _ body _) = return $ isEmptyValue body
isEmptyFn (Let binds sid body _) = withNewScope (letBindScope sid binds) $ isEmptyFn body
isEmptyFn (Block seg@(CodeSeg sid _ _) _) = withExistingScope sid $ isEmptyFn (returnValue seg)
isEmptyFn (LocalName name sid _) = do
  bind <- lookupBinding sid name
  maybe (return False) isEmptyFn (boundValue bind)
isEmptyFn (GlobalName name _ _) = do
  Def _ _ _ mbody <- lookupDef name
  maybe (return False) isEmptyFn mbody
isEmptyFn _ = return False

isEmptyValue :: Expr -> Bool
isEmptyValue (Lit UnitLit _) = True
isEmptyValue (LocalName _ _ UnitType) = True
isEmptyValue (GlobalName _ _ UnitType) = True
isEmptyValue (Let binds _ body _) = all (isEmptyValue . snd) binds && isEmptyValue body
isEmptyValue (Block seg _) = isEmptySegment seg
isEmptyValue _ = False

isEmptySegment :: CodeSeg -> Bool
isEmptySegment (CodeSeg _ stmts _) = all isEmptyStatement stmts

isEmptyStatement :: Statement -> Bool
isEmptyStatement (NakedEx ex) = isEmptyValue ex
isEmptyStatement (InitVar _ _ _ ex) = isEmptyValue ex
isEmptyStatement (SetVar _ _ ex) = isEmptyValue ex

eraseEmptyFnsExpr :: (EraseM m) => Expr -> m (TResult Expr)
eraseEmptyFnsExpr (App fn params typ) = do
  fn <- transform fn
  params <- transform params
  isEmpty <- isEmptyFn fn
  if isEmpty
    then do currentSID <- currentScopeID
            let codeSeg = CodeSeg currentSID (map NakedEx params) typ
            res <- transform $ Block codeSeg typ
            return $ TRUpdate res
    else return . TRUpdate $ App fn params typ
eraseEmptyFnsExpr (Let binds sid body typ) = do
  binds <- transform binds
  body <- withNewScope (letBindScope sid binds) $ transform body
  newBinds <- catMaybes <$> mapM eraseEmptyFn binds
  return . TRUpdate $ Let newBinds sid body typ
  where
    eraseEmptyFn bind@(LValue name _, expr) = do
      isEmpty <- isEmptyFn expr
      if isEmpty
        then do modifyU $ \u -> u { efErasedLocals = S.insert (name, sid) (efErasedLocals u) }
                return Nothing
        else return $ Just bind
eraseEmptyFnsExpr _ = return TRPassThru

eraseEmptyNamesExpr :: (EraseM m) => Expr -> m (TResult Expr)
eraseEmptyNamesExpr (LocalName name sid _) = do
  isEmpty <- getsU (S.member (name, sid) . efErasedLocals)
  if isEmpty
    then return . TRUpdate $ Lit UnitLit UnitType
    else return TRPassThru
eraseEmptyNamesExpr glob@(GlobalName _ _ _) = do
  isEmpty <- isEmptyFn glob
  if isEmpty
    then return . TRUpdate $ Lit UnitLit UnitType
    else return TRPassThru
eraseEmptyNamesExpr _ = return TRPassThru
