module Lang.GIR.Transform.EraseUnit where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.Either
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import qualified Data.Set as S
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

type EraseM m = TransM ELState m

data ELState = ELState { elUnitBinds :: S.Set (String, ScopeID) }
             deriving (Show, Eq)

emptyELState :: ELState
emptyELState = ELState S.empty

runEraseUnit :: (MonadIO m) => Package -> m (Either String Package)
runEraseUnit package = transformPackage eraseUnitTrans package emptyELState
  where
    eraseUnitTrans = noopTransformers { tnfTransformExpr = eraseUnitExpr
                                      , tnfTransformBaseType = eraseUnitType
                                      }

addUnitBind :: (EraseM m) => String -> ScopeID -> m ()
addUnitBind name sid = modifyU $ \u -> u { elUnitBinds = S.insert (name, sid) (elUnitBinds u) }

isUnitBind :: (EraseM m) => String -> ScopeID -> m Bool
isUnitBind name sid = getsU (S.member (name, sid) . elUnitBinds)

eraseUnitExpr :: (EraseM m) => Expr -> m (TResult Expr)
eraseUnitExpr (App fn params typ) = do
  fn <- transform fn
  params <- transform params
  typ <- transform typ
  let (unitExprs, newParams) = (lefts &&& rights) $ mapMaybe liftUnit params
      newApp = App fn newParams typ
  case unitExprs of
   [] -> return $ TRUpdate newApp
   _ -> do
     currentSID <- scopeID <$> currentScope
     let codeSeg = CodeSeg currentSID (map NakedEx (unitExprs ++ [newApp])) typ
     res <- transform $ Block codeSeg typ
     return $ TRUpdate res
  where
    liftUnit (Lit UnitLit _) = Nothing
    liftUnit expr =
      case underlyingType expr of
       UnitType -> Just $ Left expr
       _ -> Just $ Right expr
eraseUnitExpr (Fn params retType sid body fnType) = do
  params <- transform params
  retType <- transform retType
  body <- withNewScope (paramsScope sid params) $ transform body
  fnType <- transform fnType
  case filter (isUnit . underlyingType) params of
   [] -> return . TRUpdate $ Fn params retType sid body fnType
   unitParams -> do
     mapM_ (\(LValue name _) -> addUnitBind name sid) unitParams
     res <- transform $ Fn (filter nonUnit params) retType sid body fnType
     return $ TRUpdate res
  where
    nonUnit = not . isUnit . underlyingType
eraseUnitExpr (Let binds sid body typ) = do
  binds <- transform binds
  body <- withNewScope (letBindScope sid binds) $ transform body
  typ <- transform typ
  newBinds <- mapM liftUnit binds
  let newLet = Let (mergeBinds binds newBinds) sid body typ
  if null (filter isJust newBinds)
    then return $ TRUpdate newLet
    else TRUpdate <$> transform newLet
  where
    liftUnit (LValue name UnitType, expr) = do
      addUnitBind name sid
      return $ Just (LValue "_" UnitType, expr)
    liftUnit bind = return $ Nothing
    mergeBinds binds newBinds = map (uncurry fromMaybe) $ binds `zip` newBinds
eraseUnitExpr (LocalName name sid typ) = do
  isUnit <- isUnitBind name sid
  if isUnit
    then return . TRUpdate $ Lit UnitLit UnitType
    else TRUpdate . LocalName name sid <$> transform typ
eraseUnitExpr _ = return TRPassThru

eraseUnitType :: (EraseM m) => BaseType -> m (TResult BaseType)
eraseUnitType (FnType types retType) = do
  types <- transform types
  retType <- transform retType
  return . TRUpdate $ FnType (filter (not . isUnit) types) retType
eraseUnitType _ = return TRPassThru
