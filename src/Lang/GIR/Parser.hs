module Lang.GIR.Parser where

import           Control.Arrow
import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.State.Strict
import qualified Data.List.NonEmpty as NE
import qualified Data.Map.Strict as M
import           Data.Maybe
import qualified Data.Set as S
import           Data.String.Util
import           Data.Void
import           Lang.GIR.AST
import           Lang.GoML.Util
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import           Text.Megaparsec.Expr

type ParserT m = ParsecT Void String m

sc :: (MonadIO m) => ParserT m ()
sc = L.space space1 lineCmnt blockCmnt
  where
    lineCmnt = L.skipLineComment "--"
    blockCmnt = L.skipBlockComment "{-" "-}"

lexeme :: (MonadIO m) => ParserT m a -> ParserT m a
lexeme = L.lexeme sc

symbol :: (MonadIO m) => String -> ParserT m String
symbol = L.symbol sc

parens :: (MonadIO m) => ParserT m a -> ParserT m a
parens = between (symbol "(") (symbol ")")

braces :: (MonadIO m) => ParserT m a -> ParserT m a
braces = between (symbol "{") (symbol "}")

brackets :: (MonadIO m) => ParserT m a -> ParserT m a
brackets = between (symbol "[") (symbol "]")

integer :: (MonadIO m) => ParserT m Integer
integer = lexeme L.decimal

float :: (MonadIO m) => ParserT m Double
float = lexeme L.float

semi :: (MonadIO m) => ParserT m String
semi = symbol ";"

comma :: (MonadIO m) => ParserT m String
comma = symbol ","

dot :: (MonadIO m) => ParserT m ()
dot = reservedOp "."

idHead :: (MonadIO m) => ParserT m Char
idHead = letterChar <|> char '_'

idTail :: (MonadIO m) => ParserT m String
idTail = many (alphaNumChar <|> oneOf "_'")

name :: (MonadIO m) => ParserT m String
name = lexeme $ (:) <$> idHead <*> idTail

operator :: (MonadIO m) => ParserT m String
operator = lexeme $ some (oneOf oP_LETTERS)

identifier :: (MonadIO m) => ParserT m String
identifier = do
  -- liftIO $ putStrLn "identifier"
  ident <- name <|> operator
  when (ident `S.member` kEYWORDS || ident `elem` rESERVED_OPS) $
    failure (Just (Tokens (NE.fromList ident))) S.empty
  return ident

reserved :: (MonadIO m) => String -> ParserT m ()
reserved rname = lexeme $ try $ do
  -- liftIO $ putStrLn $ "reserved " ++ show rname
  void $ string rname
  notFollowedBy (alphaNumChar <|> oneOf "_'") <?> ("end of " ++ show rname)

reservedOp :: (MonadIO m) => String -> ParserT m ()
reservedOp op = lexeme $ try $ do
  -- liftIO $ putStrLn $ "reservedOp " ++ show op
  void $ string op
  notFollowedBy (oneOf oP_LETTERS) <?> ("end of " ++ show op)

kEYWORDS :: S.Set String
kEYWORDS = S.fromList ["package", "let", "and", "var", "in", "fn", "return", "recur", "type", "struct"]

rESERVED_OPS :: S.Set String
rESERVED_OPS = S.fromList [":","..","=","\\","|","<-","->","@","~","=>", "."]

oP_LETTERS :: String
oP_LETTERS = ":!#$%&*+./<=>?@\\^|-~"

leftToMaybe :: Either a b -> Maybe a
leftToMaybe (Left x) = Just x
leftToMaybe _ = Nothing

rightToMaybe :: Either a b -> Maybe b
rightToMaybe (Right x) = Just x
rightToMaybe _ = Nothing

leftMap :: (a -> c) -> [Either a b] -> [c]
leftMap f = mapMaybe (fmap f . leftToMaybe)

rightMap :: (b -> c) -> [Either a b] -> [c]
rightMap f = mapMaybe (fmap f . rightToMaybe)

package :: (MonadIO m) => ParserT m Package
package = do
  reserved "package"
  name <- identifier
  topLevels <- many topLevel
  eof
  return $ Package name (typeDefs topLevels) (defs topLevels)
  where
    typeDefs = namedMap . mapMaybe td
    td (TLTypeDef typ) = Just typ
    td _ = Nothing
    defs = namedMap . mapMaybe df
    df (TLDef def) = Just def
    df _ = Nothing

topLevel :: (MonadIO m) => ParserT m TopLevel
topLevel = (TLDef <$> def) <|> (TLTypeDef <$> typeDef)

newScopeID :: (MonadIO m) => ParserT m ScopeID
newScopeID = return $ ScopeID (-1)

-- newTypeVar :: (MonadIO m) => ParserT m BaseType
-- newTypeVar = TypeVar <$> nextId

def :: (MonadIO m) => ParserT m Def
def = Def <$  reserved "let"
          <*> identifier
          <*> (option [] $ brackets (name `sepBy` comma))
          <*> (optional typeAnn >>= maybe newTypeVar return)
          -- <*  liftIO (putStrLn "about to read RHS")
          <*> optional (reservedOp "=" >> expr)
          -- <*  liftIO (putStrLn "done reading RHS")

typeDef :: (MonadIO m) => ParserT m TypeDef
typeDef = do
  reserved "type"
  tname <- identifier
  TypeDef tname <$> typeArgList
                <*> (fromMaybe (Prim tname) <$> optional rhs)
  where
    rhs = id <$ reservedOp "=" <*> baseType

typeArgList :: (MonadIO m) => ParserT m [String]
typeArgList = do
  option [] (brackets (name `sepBy` comma))

expr :: (MonadIO m) => ParserT m Expr
expr = do
  res <- try appExpr <|> singleExpr
  mann <- optional typeAnn
  case mann of
   Nothing -> return res
   Just ann -> return (withType res ann)

annotatedExpr :: (MonadIO m) => ParserT m Expr
annotatedExpr = do
  withType <$> (try appExpr <|> singleExpr) <*> typeAnn

typeAnn :: (MonadIO m) => ParserT m BaseType
typeAnn = do
  -- liftIO $ putStrLn "typeAnn"
  id <$ reservedOp ":" <*> baseType

optionalAnn :: (MonadIO m) => ParserT m BaseType
optionalAnn = do
  -- liftIO $ putStrLn "optionalAnn"
  join $ option newTypeVar (return typeAnn)

appExpr :: (MonadIO m) => ParserT m Expr
appExpr = do
  -- liftIO $ putStrLn "appExpr"
  fn <- singleExpr
  -- liftIO $ putStrLn $+ "got fn:" +|+ fn
  args <- parens (expr `sepBy` comma)
  -- liftIO $ putStrLn "got params"
  App fn args <$> newTypeVar
  -- App <$> singleExpr <*> parens (expr `sepBy` comma) <*> newTypeVar

singleExpr :: (MonadIO m) => ParserT m Expr
singleExpr = try litExpr
             <|> parens expr
             <|> blockExpr
             <|> fnExpr
             <|> letExpr
             <|> try propExpr
             <|> nameExpr

blockExpr :: (MonadIO m) => ParserT m Expr
blockExpr = braces $ Block <$> codeSeg <*> newTypeVar

codeSeg :: (MonadIO m) => ParserT m CodeSeg
codeSeg = CodeSeg <$> newScopeID <*> statement `sepBy` (try newline <|> lexeme (char ';')) <*> newTypeVar

statement :: (MonadIO m) => ParserT m Statement
statement = initVar <|> try setVar <|> naked
  where
    initVar = InitVar <$ reserved "var" <*> identifier <*> typeAnn <*> newScopeID <* reservedOp "=" <*> expr
    setVar = SetVar <$> identifier <*> newScopeID <* reservedOp "=" <*> expr
    naked = NakedEx <$> expr

fnExpr :: (MonadIO m) => ParserT m Expr
-- fnExpr = Fn <$  reserved "fn"
--             <*> parens (lvalueOpTy `sepBy` comma)
--             <*> returnTyp
--             <*> newScopeID
--             <*> blockExpr
--             <*> newTypeVar
--   where
--     returnTyp = fromMaybe UnitType <$> optional (id <$ reservedOp "->" <*> baseType)
fnExpr = do
  reserved "fn"
  -- liftIO $ putStrLn "read \"fn\""
  params <- parens ((LValue <$> identifier <*> (optional typeAnn >>= maybe newTypeVar return)) `sepBy` comma)
  -- liftIO $ putStrLn $ "read params: " ++ show params
  returnTyp <- option UnitType (reservedOp "->" >> baseType)
  -- liftIO $ putStrLn $ "read returnTyp: " ++ pp returnTyp
  sid <- newScopeID
  -- liftIO $ putStrLn $ "read sid: " ++ show sid
  body <- blockExpr
  -- liftIO $ putStrLn $ "read body: " ++ pp body
  fnType <- newTypeVar
  -- getPosition >>= liftIO . print
  return $ Fn params returnTyp sid body fnType

lvalue :: (MonadIO m) => ParserT m LValue
lvalue = LValue <$> identifier <* reservedOp ":" <*> baseType

lvalueOpTy :: (MonadIO m) => ParserT m LValue
lvalueOpTy = do
  -- liftIO $ putStrLn "lvalueOpTy"
  LValue <$> identifier <*> (optional typeAnn >>= maybe newTypeVar return)

letExpr :: (MonadIO m) => ParserT m Expr
letExpr = do
  -- liftIO $ putStrLn "letExpr"
  Let <$> between (reserved "let") (reserved "in") (letBind `sepBy` reserved "and")
      <*> newScopeID
      <*> expr
      <*> newTypeVar
  where
    letBind = (,) <$> lvalueOpTy
                  <*  reservedOp "="
                  <*> expr

propExpr :: (MonadIO m) => ParserT m Expr
propExpr = Prop <$> (parens expr <|> nameExpr) <* dot <*> name <*> newTypeVar

stringExpr :: (MonadIO m) => ParserT m String
stringExpr = lexeme $ id <$ char '"' <*> manyTill L.charLiteral (char '"')

charExpr :: (MonadIO m) => ParserT m Char
charExpr = lexeme $ id <$ char '\'' <*> L.charLiteral <* char '\''

litExpr :: (MonadIO m) => ParserT m Expr
litExpr = Lit <$> (try (FloatLit <$> float)
                   <|> try (IntLit <$> integer)
                   <|> try (StringLit <$> stringExpr)
                   <|> try (CharLit <$> charExpr)
                   <|> (id <$ symbol "(" <* symbol ")" <*> pure UnitLit))
              <*> newTypeVar

typeArgsApp :: (MonadIO m) => ParserT m [BaseType]
typeArgsApp = do
  margs <- optional (brackets (baseType `sepBy` comma))
  case margs of
   Nothing -> return []
   Just args -> return args

nameExpr :: (MonadIO m) => ParserT m Expr
nameExpr = GlobalName <$> identifier
                      <*> typeArgsApp
                      <*> newTypeVar

baseType :: (MonadIO m) => ParserT m BaseType
baseType = do
  types <- singleType `sepBy1` reservedOp "->"
  case types of
   [typ] -> return typ
   typs -> return $ FnType (init typs) (last typs)

singleType :: (MonadIO m) => ParserT m BaseType
singleType = try unitType
             <|> parens baseType
             <|> structType
             <|> typeName

unitType :: (MonadIO m) => ParserT m BaseType
unitType = id <$ symbol "(" <* symbol ")" <*> pure UnitType

typeName :: (MonadIO m) => ParserT m BaseType
typeName = TypeName <$> identifier <*> typeArgsApp

structType :: (MonadIO m) => ParserT m BaseType
structType = StructType <$ reserved "struct" <*> lvalue `sepBy` comma
