module Lang.GIR.AST.Util ( module Lang.GIR.AST.Util
                         , module Lang.GoML.Util
                         , module Lang.GoML.AST
                         ) where

import Data.List
import Lang.GoML.AST (Literal(..))
import Lang.GoML.Util (Named(..), namedMap, namedPairs, nextId)

targStr :: [String] -> String
targStr [] = ""
targStr targs = "[" ++ intercalate "," targs ++ "]"
