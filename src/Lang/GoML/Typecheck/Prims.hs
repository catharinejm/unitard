module Lang.GoML.Typecheck.Prims (prims) where

import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Lang.GoML.AST
import           Lang.GoML.Util

mkTypeMap :: [(String, Prim)] -> Map String TypeDef
mkTypeMap = M.fromList . map (\(n, p) -> (n, TypeDef nonEID n (emptySchema (Prim p))))

signedIntTypes :: Map String TypeDef
signedIntTypes = mkTypeMap [ ("int", IntT)
                           , ("int8", Int8T)
                           , ("int16", Int16T)
                           , ("int32", Int32T)
                           , ("int64", Int64T)
                           , ("intptr", IntPtrT)
                           ]

unsignedIntTypes :: Map String TypeDef
unsignedIntTypes = mkTypeMap [ ("uint", UIntT)
                             , ("uint8", UInt8T)
                             , ("uint16", UInt16T)
                             , ("uint32", UInt32T)
                             , ("uint64", UInt64T)
                             , ("uintptr", UIntPtrT)
                             ]

intTypes :: Map String TypeDef
intTypes = signedIntTypes `M.union` unsignedIntTypes

floatTypes :: Map String TypeDef
floatTypes = mkTypeMap [("float32", Float32T), ("float64", Float64T)]

numberTypes :: Map String TypeDef
numberTypes = intTypes `M.union` floatTypes

boolType :: Map String TypeDef
boolType = mkTypeMap [("bool", BoolT)]

stringType :: Map String TypeDef
stringType = mkTypeMap [("string", StringT)]

typeAliases :: Map String TypeDef
typeAliases = M.fromList [ ("rune", intTypes M.! "int32")
                         , ("byte", intTypes M.! "uint8")
                         ]

prims :: Map String TypeDef
prims = numberTypes `M.union` boolType `M.union` stringType `M.union` typeAliases
