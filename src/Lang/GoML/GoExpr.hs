module Lang.GoML.GoExpr where

import Lang.GoML.AST
import Lang.GoML.Interregnum
import Lang.GoML.Util

data GoStatement = GoVarInit EID String GoType (Maybe GoExpr)
                 | GoVarAssign EID String GoExpr
                 | GoReturn EID [GoExpr]
                 | GoExpr GoExpr
                 deriving (Show, Eq)

data GoExpr = GoName EID String
            | GoApp EID GoExpr [GoExpr]
            | GoProp EID GoExpr String
            | GoMethod EID GoExpr String [GoExpr]
            | GoPositionalCtor EID String [GoExpr]
            | GoNamedCtor EID String [(String, GoExpr)]
            | GoFunc EID [(String, GoType)] [GoType] [GoStatement]
            deriving (Show, Eq)

data GoType = GoPrim Prim
            | GoNamedType String
            | GoStruct [(String, GoType)]
            | GoInterface [GoSig]
            | GoFuncType [(String, GoType)] [GoType]
            | GoSlice GoType
            deriving (Show, Eq)

data GoSig = GoSig [(String, GoType)] [GoType]
           deriving (Show, Eq)
data GoFuncDecl = GoFuncDecl String
                  (Maybe (String, BaseType)) [(String, BaseType)] [BaseType]
                  [GoStatement]
                deriving (Show, Eq)
data GoVarDecl = GoVarDecl String GoType (Maybe GoExpr)
                deriving (Show, Eq)
data GoTypeDef = GoTypeDef String GoType

-- buildGoAST :: (MonadIO m) => IRPackage -> 
