module Lang.GoML.Env where

import           Control.Arrow
import qualified Data.Map.Strict as M
import           Lang.GoML.AST
import           Lang.GoML.Util

data Env = Env { packageName   :: PackageName
               , envImports    :: M.Map String Import
               , functionTypes :: M.Map String TypeSchema
               , functionDecls :: M.Map String FunDecl
               , typeDefs      :: M.Map String TypeDef
               , localNames    :: M.Map String BaseType
               , fixities      :: M.Map String Fixity
               , envExprTypes  :: M.Map EID BaseType
               }
         deriving (Show)

emptyEnv :: PackageName -> [Import] -> Env
emptyEnv name imports =
  Env { packageName   = name
      , envImports    = M.fromList $ map (nameOf &&& id) imports
      , functionTypes = M.empty
      , functionDecls = M.empty
      , typeDefs      = M.empty
      , localNames    = M.empty
      , fixities      = M.empty
      , envExprTypes  = M.empty
      }
