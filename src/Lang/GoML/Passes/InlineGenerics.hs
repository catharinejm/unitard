module Lang.GoML.Passes.InlineGenerics where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Interregnum hiding (withNewScope)
import           Lang.GoML.Passes.Transform
import           Lang.GoML.Util

data ILState = ILState { ilsCurrentInlines :: [(String, IRScopeIdx)]
                       }
             deriving (Show, Eq)

emptyILState :: ILState
emptyILState = ILState { ilsCurrentInlines = []
                       }

type InlineM m = TransM ILState m

inliner :: Transformer ILState
inliner = noopTransformer { tnsTransformStatement = Just inlineStatement
                          }

runInliner :: (MonadIO m) => IRPackage -> m (Either String IRPackage)
runInliner pkg = runTransformer inliner pkg emptyILState

lookupLocal :: (InlineM m) => IRScopeIdx -> String -> m IRBinding
lookupLocal idx name = do
  TEnv{..} <- ask
  liftIO $ putStrLn $ take 80 (repeat '*')
  liftIO $ putStrLn $+ "lookupLocal" +|+ idx +|+ name
  liftIO $ putStrLn $+ map tos teScopes
  liftIO $ putStrLn $ take 80 (repeat '*')
  scope <- getScope idx
  case irScopeVars scope M.!? name of
   Nothing -> throwError $+ "undefined local name:" +|+ name +|+ "(scope" +|+ idx +:+ ")"
   Just b -> return b

chaseGeneric :: (InlineM m) => IRExpr -> m (Maybe (String, BaseType))
chaseGeneric (IRGlobalName _ name types (TypeSchema tvs@(_:_) typ)) = do
  substT <- substType (M.fromList (tvs `zip` types)) typ
  return $ Just (name, substT)
chaseGeneric (IRLocalName _ idx name _) = do
  bind <- lookupLocal idx name
  case bind of
   IRLetBind' (IRLetBind _ _ expr) -> chaseGeneric expr
   _ -> return Nothing
chaseGeneric _ = return Nothing

inlineStatement :: (InlineM m) => IRStatement -> m (TResult IRStatement)
inlineStatement (IRReturn app@(IRApp _ _ _ _)) = do
  app'@(IRApp _ fn params _) <- transformExpr app
  mglob <- chaseGeneric fn
  case mglob of
   Nothing -> defaultT
   Just (name, (FunType (Sig _ ret))) -> do
     mcur <- gets (fmap snd . find ((==name) . fst) . ilsCurrentInlines)
     case mcur of
      Nothing -> joinT $ IRReturn <$> inlineRewrite name app'
      Just scopeIdx -> returnT $ IRRecur scopeIdx params ret
inlineStatement _ = defaultT

type SubState = (IRScopeIdx, M.Map String BaseType)
type SubstM m = TransM SubState m

substTransformer :: Transformer SubState
substTransformer = noopTransformer { tnsTransformType = Just substParam
                                   , tnsTransformScopeIdx = Just substScopeIdx
                                   }
substParam :: (SubstM m) => BaseType -> m (TResult BaseType)
substParam (TypeParam a) = do
  mparam <- gets ((M.!? a) . snd)
  case mparam of
   Nothing -> throwError $+ "no binding for type param:" +|+ a
   Just p -> returnT p
substParam _ = defaultT

substScopeIdx :: (SubstM m) => IRScopeIdx -> m (TResult IRScopeIdx)
substScopeIdx idx = joinT $ (idx+) <$> gets fst

type Subst m a = Trans m SubState a
runSubst :: (InlineM m, Stringable a) => M.Map String BaseType -> Subst m a -> m a
runSubst varMap subst = do
  liftIO $ putStrLn $+ "runSubst (" +:+ S varMap +:+ ")"
  env <- asks $ \e -> e { teTransformer = substTransformer }
  curScope <- asks teCurrentScope
  res <- runReaderT (evalStateT (runExceptT subst) (curScope, varMap)) env
  case res of
   Left err -> throwError err
   Right res -> do liftIO $ putStrLn $+ " -->" +|+ res
                   return res

substType :: (InlineM m) => M.Map String BaseType -> BaseType -> m BaseType
substType varMap = runSubst varMap . transformType

substStatement :: (InlineM m) => M.Map String BaseType -> IRStatement -> m IRStatement
substStatement varMap = runSubst varMap . transformStatement

lookupGenericDef :: (InlineM m) => String -> m IRGlobalDef
lookupGenericDef name = do
  mdef <- asks ((M.!? name) . irGlobals . tePackage)
  case mdef of
   Nothing -> throwError $+ "undefined name:" +|+ name
   Just def -> return def

withInline :: (InlineM m) => String -> m a -> m a
withInline fname action = do
  pushInline
  res <- safeAction
  popInline
  rethrow res
  where
    pushInline = do sid <- asks teCurrentScope
                    modify $ \s -> s { ilsCurrentInlines = (fname, sid) : ilsCurrentInlines s }
    popInline = modify $ \s -> s { ilsCurrentInlines = tail (ilsCurrentInlines s) }
    safeAction = (Right <$> action) `catchError` (pure . Left)
    rethrow (Left err) = throwError err
    rethrow (Right res) = return res

inlineRewrite :: (InlineM m) => String -> IRExpr -> m IRExpr
inlineRewrite fname app@(IRApp eid _ args typ) = do
  liftIO $ putStrLn $+ "inlineRewrite" +|+ fname +|+ app
  gen <- lookupGenericDef fname
  let IRGlobalFun _ scope _ params (TypeSchema tvs _) body = case gen of
                                                              IRGlobalFun _ _ _ _ _ _ -> gen
                                                              other -> error $+ "NOT FUN:" +|+ other
  letBinds <- mapM (uncurry mkBind) $ params `zip` args
  (newBody, newScope) <- withNewScope scope $ withInline fname $ do
    substBody <- substStatement (M.fromList $ tvs `zip` map underlyingType args) body
    let bindMap = M.fromList $ map (nameOf &&& IRLetBind') letBinds
    (,) <$> transformStatement substBody <*> (IRScope <$> asks ((+1) . teCurrentScope) <*> pure bindMap)
  return $ IRLet eid newScope letBinds newBody typ
  where
    mkBind (IRFunParam idx n typ) e = do
      liftIO $ putStrLn $+ "mkBind (param[" +:+ idx +:+ "]" +|+ n +|+ ":" +|+ typ +:+ ")" +|+ e
      let res = IRLetBind n (underlyingType e) e
      liftIO $ putStrLn $+ " -->" +|+ "IRLetBind" +|+ n +|+ underlyingType e +|+ e
      return res
inlineRewrite _ other = return other
